function validarFormulario() {
  var usuario = document.getElementById("correo").value;
  if (usuario == "") {
    var text = document.createTextNode("No se ha registrado un correo");
    document.getElementById("error").style.color = "#FF0000";
    document.getElementById("error").appendChild(text);
    setTimeout(() => {
      // Eliminando todos los hijos de un elemento
      var ele = document.getElementById("error");
      while (ele.firstChild) {
        ele.removeChild(ele.firstChild);
      }
    }, 2000);
  } else {
    var text = document.createTextNode(
      "!Estaras pendiente de nuestras noticias!"
    );
    document.getElementById("error").style.color = "#00ff00";
    document.getElementById("error").appendChild(text);
    setTimeout(() => {
      // Eliminando todos los hijos de un elemento
      var ele = document.getElementById("error");
      while (ele.firstChild) {
        ele.removeChild(ele.firstChild);
      }
    }, 2000);
  }
}
